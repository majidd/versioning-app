FROM python:3.5
RUN pip install flask
COPY ./source .
ENV VERSIONING=latest
WORKDIR .
CMD ["python3", "./run.py"]

