# This is a simple flask api which returns the project versioning number in this address: `/v1/version/`
##### You can run this simply by `docker run -d -p 4000:4000 registry.gitlab.com/majidd/versioning-app/myapp:v1.0.0-stable` and send a reaquest to the api by `curl localhost:4000/v1/version`

## CICD Pipleline
  The CI/CD has three stages: build, deploy and, release. In the build stage the docker image is built based on the tag name which is created. Then the built image is pushed to the container registry. After that, in the deploy stage the latest docker image is going to be deployed to a kubernetes environment using a blue/green deployment.
  
  The **deployment.yaml**, **service.yaml** and, **ingress.yaml** files is placed in the **/manifests/** directory. These two stages are **triggered by creating a semantic tag** which has a **stable** notation in the end of its name.
  
  In the release stage a release will be created based on the commit messages and tag messages using a third party tool (**release-cli**). The created packages and assets is availble in the release section of the project. This stage is **triggered when a semantic tag** is created which has a **release** notation the end of its name.  

## Production server considerations
A kubernetes cluster must be implemented and integrated with this project on the gitlab. The integrated kubernetes environment must be named **production**. 

In order to pull the images from this registry, a kubernetes **pullsecret** name pullsecret must be created using the gitlab instance credentials. 

An ingress controller must be deployed and ready to route external traffic using our created ingress resource to the desired service and pod, for example traefik or nginx. 