from flask import Flask
import os
app = Flask(__name__)

version = os.getenv('VERSIONING')


@app.route('/v1/version')
def index():
    return version

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4000, debug=True)

